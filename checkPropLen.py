#!/bin/env python3
import glob

# Check the size of new property (limited to 20)
noerrors = 1
for file_name in glob.glob("./*.pvs"):
    with open(file_name) as f:
        linen = 0
        for l in f.readlines():
            l = l.split("\n")[0]
            prop = l.split(")")[-1]
            if len(prop) > 20:
                print("Property invalid. File Name %s Line number %d Property %s" % (file_name, linen, prop))
                noerrors = 0
            linen+=1

if noerrors:
    print("All properties are valid!")
